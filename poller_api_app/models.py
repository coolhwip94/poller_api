from django.db import models

# Create your models here.


class FruitPrices(models.Model):
    '''
    Example of poller entry
    '''
    name = models.CharField(max_length=200, blank=False)
    description = models.TextField(blank=True)
    price = models.IntegerField(blank=True, default=0)

    def __str__(self):
        return self.name


class CryptoPrices(models.Model):
    '''
    Model for crypto prices
    '''
    name = models.CharField(max_length=200, blank=False)
    description = models.TextField(blank=True)
    price = models.FloatField(blank=False, default=0)
    symbol = models.CharField(max_length=10, blank=False)

    def is_latest_price(self):
        '''
        Determine if price instance is the latest one for that specific symbol
        '''
        is_latest_price = False
        try:
            crypto_prices_queryset = CryptoPrices.objects.all().filter(symbol=self.symbol)

            # queryset doesnt allow negative index
            prices_list = [  # add queryset result to new list
                crypto_price for crypto_price in crypto_prices_queryset.values_list()]
            print(prices_list)
            if prices_list[-1][0] == self.id:
                is_latest_price = True
        except Exception:
            is_latest_price = False

        return is_latest_price

    def __str__(self):
        if self.is_latest_price():
            return self.symbol.upper() + '_' + 'latest'
        return self.symbol.upper()
