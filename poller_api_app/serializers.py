from django.contrib.auth.models import User, Group
from rest_framework import serializers
from .models import FruitPrices, CryptoPrices


class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = ['url', 'username', 'email', 'groups']


class GroupSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Group
        fields = ['url', 'name']


class FruitPricesSerializer(serializers.ModelSerializer):
    class Meta:
        model = FruitPrices
        fields = ['id', 'name', 'description', 'price']


class CryptoPricesSerializer(serializers.ModelSerializer):

    class Meta:
        model = CryptoPrices
        fields = ['id', 'name', 'description',
                  'price', 'symbol', 'is_latest_price']
