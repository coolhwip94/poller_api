from django.apps import AppConfig


class PollerApiAppConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'poller_api_app'
