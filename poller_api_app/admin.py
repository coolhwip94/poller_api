from django.contrib import admin
from .models import CryptoPrices, FruitPrices

# Register your models here.
admin.site.register(CryptoPrices)
admin.site.register(FruitPrices)
