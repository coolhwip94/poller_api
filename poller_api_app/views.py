from django.contrib.auth.models import User, Group
from rest_framework import viewsets
from rest_framework import permissions
from poller_api_app.serializers import UserSerializer, GroupSerializer, FruitPricesSerializer, CryptoPricesSerializer
from .models import FruitPrices, CryptoPrices
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import filters


# Create your views here.


class UserViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = User.objects.all().order_by('-date_joined')
    serializer_class = UserSerializer
    permission_classes = [permissions.IsAuthenticated]


class GroupViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows groups to be viewed or edited.
    """
    queryset = Group.objects.all()
    serializer_class = GroupSerializer
    permission_classes = [permissions.IsAuthenticated]


class FruitPricesViewSet(viewsets.ModelViewSet):
    queryset = FruitPrices.objects.all()
    serializer_class = FruitPricesSerializer


class CryptoPricesViewSet(viewsets.ModelViewSet):
    queryset = CryptoPrices.objects.all()
    serializer_class = CryptoPricesSerializer

    # filters
    filter_backends = [DjangoFilterBackend, filters.OrderingFilter]
    filterset_fields = ['symbol', 'id']

    # define what can be used for ordering (example : GET /cryptoprices/?ordering=-id)
    ordering_fields = '__all__'

    # define default ordering
    ordering = ['id']
