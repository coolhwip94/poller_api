from django.urls import path
from rest_framework import routers
from django.conf.urls import include
from . import views

router = routers.DefaultRouter()
router.register('users', views.UserViewSet)
router.register('groups', views.GroupViewSet)
router.register('fruitprices', views.FruitPricesViewSet)
router.register('cryptoprices', views.CryptoPricesViewSet)

urlpatterns = [
    path('', include(router.urls)),
]
