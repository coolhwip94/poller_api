import configparser
config = configparser.ConfigParser(interpolation=None)
config.read_file(open('app_setup.cfg'))

# django configs
db_host = config['django']['db_host']
db_name = config['django']['db_name']
db_password = config['django']['db_password']
db_username = config['django']['db_username']
secret_key = config['django']['secret_key']
